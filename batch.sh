cd mkecc-slurm-cluster
echo "Building mkecc slurm docker Image"
sudo docker build -t localhost:5000/mkeccslurm .
sudo docker push localhost:5000/mkeccslurm
cd ..
echo "Cleaning up previous deployment"
kubectl delete -f k8s_slurm/

echo "Deploying Slurm Cluster"
kubectl create -f k8s_slurm/
sleep 1m
kubectl get pods -o wide