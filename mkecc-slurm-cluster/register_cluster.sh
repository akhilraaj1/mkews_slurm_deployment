#!/bin/bash

# Author = Gunjan Sethi gunjan@magikeye.com 
# Version = 1.0

set -e

sudo docker exec slurmctld bash -c "/usr/bin/sacctmgr --immediate add cluster name=linux" && \
sudo docker-compose restart slurmdbd slurmctld