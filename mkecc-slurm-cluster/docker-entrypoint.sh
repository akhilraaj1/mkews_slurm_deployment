#!/bin/bash

set -e

if [ $HOSTNAME = "slurmdbd" ]
then
    echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start

    echo "---> Starting the Slurm Database Daemon (slurmdbd) ..."

    {
        until echo "SELECT 1" | mysql -h mysql -u slurm -ppassword 2>&1 > /dev/null
        do
            echo "-- Waiting for database to become active ..."
            sleep 2
        done
    }
    echo "-- Database is now active ..."
    /etc/init.d/slurmdbd start
    sleep 2m
    /etc/init.d/slurmdbd restart
    /bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
fi

if [ $HOSTNAME = "slurmctld" ]
then
    # echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start
    /etc/init.d/slurmctld start
    sleep 30
    /usr/bin/sacctmgr --immediate add cluster name=linux
    sleep 30
    /etc/init.d/slurmctld restart
    /bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
fi

if [ $HOSTNAME = "worker1" ]
then

    echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start
    /etc/init.d/slurmd start
    /bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
fi
if [ $HOSTNAME = "worker2" ]
then

    echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start
    /etc/init.d/slurmd start
    /bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
fi
#/bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
exec "$@"