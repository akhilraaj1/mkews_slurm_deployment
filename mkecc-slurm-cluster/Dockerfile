FROM ubuntu:20.04

LABEL maintainer="Gunjan Sethi gunjan@magikeye.com" version="1.0"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get -yqq  dist-upgrade

# Dependancy packages install
RUN apt-get install -yqq bzip2 curl gcc gfortran iputils-ping \
libmariadb-dev libmunge2 libmunge-dev libopenmpi-dev \
lua5.3 lua5.3-dev make mariadb-client mariadb-server munge \
openmpi-bin psmisc python python3-mysqldb python3-pip python-dev \
python-numpy python-psutil rsyslog software-properties-common \
sudo supervisor vim git

# Slurm-specific packages install
RUN apt-get install slurmdbd slurm slurm-wlm -yqq

# Copy SLURM configuration files
ADD /SLURM-Config-Files/slurmdbd.conf /etc/slurm-llnl
ADD /SLURM-Config-Files/slurm.conf /etc/slurm-llnl

# Enable spooling
RUN mkdir -p /var/spool/slurm-llnl \
    && mkdir -p /var/run/slurm-llnl \
    && mkdir data \
    && chown -R slurm:slurm /var/*/slurm* \
    && chown -R slurm:slurm /data
    

# Enable Munge key authentication
RUN git clone https://akhilraaj1@bitbucket.org/akhilraaj1/mungekey.git
RUN mv mungekey/munge.key /etc/munge/
RUN chmod a-r /etc/munge/munge.key \
    && chmod u-w /etc/munge/munge.key \
    && chmod u+r /etc/munge/munge.key \
    && sudo chown munge:munge /etc/munge/munge.key \
    && chmod +x /etc/init.d/munge

## Install controller and worker packages.
# pip3 install <package-names>

## If you don't have access to the above packages, you can test the cluster with
# some sample data. Uncomment the below lines and add the /data folder into this current folder.

# RUN mkdir data
# COPY data/ data/

# Entrypoint script for containers
EXPOSE 22
EXPOSE 443 6817 6818 6819
COPY docker-entrypoint.sh /usr/local/bin
ENTRYPOINT ["/bin/bash","/usr/local/bin/docker-entrypoint.sh"]

