#!/bin/sh

set -e

#cd /mkews_web/mkews/Application/MagikEye_WebInterface/
#python3 manage.py collectstatic --noinput
#uwsgi --socket :8000 --master --enable-threads --module connect.wsgi
gunicorn --chdir /mkews_web/mkews/Application/MagikEye_WebInterface/ connect.wsgi:application --workers=4 --threads=4 --worker-class=gthread --bind :8000 & sh ./startdbapi.sh