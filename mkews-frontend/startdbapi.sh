#!/bin/bash

set -e

cd /mkews_web/mkews/Application/MagikEye_BackEnd/
sleep 5
#python3 manage.py collectstatic --noinput
#uwsgi --http :8002 --master --enable-threads --module magikeye.wsgi
PYTHONPATH=$PYTHONPATH:/mkews_web:/mkews_web/mkews/ gunicorn magikeye.wsgi:application --workers=4 --threads=4 --worker-class=gthread --bind :8002